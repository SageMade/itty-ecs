#pragma once
#include "Math.h"
#include <string>
#include <vector>
#include <set>
#include <typeindex>

#define MATRIX_TYPE mat4
#define STRING_TYPE std::string

/*
 * A minimal ECS system for rapid game prototyping. Note that there is no memory management or advanced systems provided
 * This is not meant for production code without HEAVY modifications
 * Author: Sage Matthews
 * Date: April 19th, 2020
 */

struct GameObject;
class GameScene;

/*
 * Represents a component that can be attached to an object to add behaviour to it
 */
class IGameComponent {
public:
	IGameComponent() : _gameObject(nullptr) { }
	virtual ~IGameComponent() = default;
	
	virtual void Update(float deltaMS) { };
	virtual void Render() { }
	virtual void OnInit() { }
	virtual void OnDestroy() { }

	GameObject* Object() const { return _gameObject; }

private:
	friend struct GameObject;
	GameObject* _gameObject;
};

/*
 * Represents a collection of components, as well as a container for child objects. All game objects have a transform and a
 * debug name
 */
struct GameObject {
	MATRIX_TYPE Transform;
	STRING_TYPE DebugName;

	GameObject() : DebugName("<unknown>"), _parent(nullptr) { }

	inline GameObject* AddChild() {
		GameObject* child = new GameObject();
		_children.insert(child);
		return child;
	}
	size_t ChildCount() const { return _children.size(); }
	size_t ComponentCount() const { return _components.size(); }
	
	template <typename ComponentType, typename = typename std::enable_if<std::is_base_of<IGameComponent, ComponentType>::value>::type>
	ComponentType* AddComponent() {
		assert(!HasComponent<ComponentType>());
		IGameComponent* result = new ComponentType();
		result->_gameObject = this;
		_components.insert(result);
		return static_cast<ComponentType*>(result);
	}

	template <typename ComponentType, typename = typename std::enable_if<std::is_base_of<IGameComponent, ComponentType>::value>::type>
	ComponentType* FetchOrAssign() {
		ComponentType* result = nullptr;
		if (!TryGetComponent<ComponentType>(result)) {
			result = new ComponentType();
			static_cast<IGameComponent*>(result)->_gameObject = this;
			_components.insert(static_cast<IGameComponent*>(result));
		}
		return result;
	}

	template <typename ComponentType, typename = typename std::enable_if<std::is_base_of<IGameComponent, ComponentType>::value>::type>
	bool RemoveComponent() {
		ComponentType* result = nullptr;
		if (TryGetComponent<ComponentType>(result)) {
			delete result;
			_components.erase(result);
			return true;
		}
		return false;
	}

	template <typename ComponentType, typename = typename std::enable_if<std::is_base_of<IGameComponent, ComponentType>::value>::type>
	bool HasComponent() {
		for(const auto& ptr : _components) {
			if (std::type_index(typeid(*ptr)) == std::type_index(typeid(ComponentType)))
				return true;
		}
		return false;
	}

	template <typename ComponentType, typename = typename std::enable_if<std::is_base_of<IGameComponent, ComponentType>::value>::type>
	bool TryGetComponent(ComponentType*& result) {
		for (IGameComponent* ptr : _components) {
			if (std::type_index(typeid(*ptr)) == std::type_index(typeid(ComponentType))) {
				ComponentType* castPtr = static_cast<ComponentType*>(ptr);
				result = castPtr;
				return true;
			}
		}
		return false;
	}

private:
	friend class GameScene;
	friend class IGameComponent;
	std::set<IGameComponent*> _components;
	std::set<GameObject*> _children;
	GameObject* _parent;
};

/*
 * Represents a scene consisting of root game objects. This is simply a container class for the root
 * game objects for your scene hierarchy
 */
class GameScene
{
public:
	inline void Update(float deltaMS) {
		for (GameObject* object : _objects) {
			__UpdateGameObject(object, deltaMS);
		}

		for (GameObject* object : _destructionQueue) {
			for (IGameComponent* component : object->_components) {
				component->OnDestroy();
				delete component;
			}
			if (object->_parent != nullptr) {
				object->_parent->_children.erase(object);
			}
			else {
				_objects.erase(object);
			}
			delete object;
		}
	}
	inline void Render() {
		for (GameObject* object : _objects) { __RenderGameObject(object); }
	}
	inline void Init() {
		for (GameObject* object : _objects) { __InitGameObject(object); }
	}
	
	inline GameObject* CreateObject(const STRING_TYPE& debugName = "<unknown>") {
		GameObject* result = new GameObject(); // TODO: Memory management
		result->DebugName = debugName;
		_objects.insert(result);
		return result;
	}
	inline bool TryGetObject(const STRING_TYPE& debugName, GameObject*& result) {
		for (GameObject* object : _objects) {
			if (debugName == object->DebugName) {
				result = object;
				return true;
			}
		}
		return false;
	}
	inline void Destroy(GameObject* gameObject) { _destructionQueue.insert(gameObject);	}

private:
	std::set<GameObject*> _objects;
	std::set<GameObject*> _destructionQueue;

	inline static void __UpdateGameObject(GameObject* object, float deltaMS) {
		for (IGameComponent* component : object->_components) {
			component->Update(deltaMS);
		}
		for (GameObject* child : object->_children) {
			__UpdateGameObject(child, deltaMS);
		}
	}
	inline static void __RenderGameObject(GameObject* object) {
		//LineRenderer3D::PushModelMatrix(object->Transform);
		for (IGameComponent* component : object->_components) {
			component->Render();
		}
		for (GameObject* child : object->_children) {
			__RenderGameObject(child);
		}
		//LineRenderer3D::PopModelMatrix();
	}
	inline static void __InitGameObject(GameObject* object) {
		for (IGameComponent* component : object->_components) {
			component->OnInit();
		}
		for (GameObject* child : object->_children) {
			__InitGameObject(child);
		}
	}
};