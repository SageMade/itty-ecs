# Itty ECS

Itty ECS is a single file minimal starter for an Entity Component System for games. This is meant as a starting point for rapid prototyping of games, and is *not* recommended for use in production code. There is no fancy memory management and there is no cache coherency or references.

## Usage

To get started using Itty ECS, you will need to include a math library such as Itty Math or GLM. This math library should support 4x4 matrices. You can then modify the define for `MATRIX_TYPE` to be the fully qualified type name of your mat4.

Additionally, if you are using custom string implementations, you can modify the `STRING_TYPE` define to your custom type. It should be castable from a const char* and support the == operator for equality checking.

Scenes can be created by creating an instance of `GameScene`, and adding game objects to it with `GameScene::CreateObject`. Components can be added to these game objects by calling `GameObject::AddComponent<Type>()`, or with `GameObject::FetchOrAssign<Type>()` (which will return the existing component, or create it if none exists). After adding all objects to your scene, you should call `GameScene::Init()` before making any calls to `Update` or `Render`.

## Overview

Itty ECS has 3 basic classes:

* IGameComponent --> This is the component interface that behaviors and other components should derive from
* GameObject --> This acts as a container for components, children, as well as forms the basis of a transformation hierarchy
* GameScene --> Stores the root GameObjects of a given scene, and allows for creation, deletion, and method calls on objects

Note that GameObject makes use of c++17 template restrictions and type_index to allow adding and removing of objects that extend from the IGameComponent interface. Only a single instance of a given component type can be attached to a game object, and components can only ever be attached to a single object. You should *NOT* be creating instances of your components in your own code, you should always be using `GameObject::AddComponent` or similar functions.

Itty ECS follows the design principle that components *are* behavior, in a similar fashion to Unity's MonoBehaviour system. Future improvements will focus on adding better support for `Systems`, to allow for sorting and iterating over a component type, for instance to iterate over all renderable components in a scene.

## Future Improvements

* Implement a shadow registry for creating prefab objects
* Support for object cloning/duplication
* Cache-coherent memory management
* More strongly defined concepts of Systems (more tools to move behavior away from components when desired)

## License

Itty ECS uses the MIT license, which means that you are free to copy, modify, improve, redistribute, etc... as you see fit. If you make improvements to it, it would always be a good idea to submit a PR though and contribute back to the project though!
